var gulp = require('gulp');
var inject = require('gulp-inject');
var wiredep = require('wiredep').stream;
var Server = require('karma').Server;
var clean = require('gulp-clean');
var install = require("gulp-install");

gulp.task('clean', function () {
    return gulp.src(['./node_modules','./src/main/resources/static/bower_components'], {read: false})
        .pipe(clean());
});

gulp.task('installDependencies', ['clean'], function () {
    gulp.src(['./package.json', './bower.json'])
        .pipe(install());
});

gulp.task('loadJS', ['loadBowerComponent'], function () {
    var target = gulp.src('./src/main/resources/templates/index.html');
    // It's not necessary to read the files (will speed up things), we're only after their paths:
    var sources = gulp.src([
        './src/main/resources/static/js/*.js',
        './src/main/resources/static/js/config/*.js',
        './src/main/resources/static/js/controller/*.js',
        './src/main/resources/static/js/directive/*.js',
        './src/main/resources/static/js/service/*.js',
        './src/main/resources/static/js/lib/*.js'
    ], {read: false});

    return target.pipe(inject(sources, {ignorePath: 'src/main/resources/static'}))
        .pipe(gulp.dest('./src/main/resources/templates'));
});

gulp.task('loadBowerComponent', function () {
    return gulp.src('./src/main/resources/templates/index.html')
        .pipe(wiredep({ignorePath: '../static'}))
        .pipe(gulp.dest('./src/main/resources/templates'));
});


gulp.task('loadCSS', ['loadJS', 'loadBowerComponent'], function () {
    var target = gulp.src('./src/main/resources/templates/index.html');
    // It's not necessary to read the files (will speed up things), we're only after their paths:
    var sources = gulp.src([
        './src/main/resources/static/css/*.css'
    ], {read: false});

    return target.pipe(inject(sources, {ignorePath: 'src/main/resources/static'}))
        .pipe(gulp.dest('./src/main/resources/templates'));
});






gulp.task('testJS', function () {
    var target = gulp.src('./karma.conf.js');

    var sources = gulp.src([
        './src/main/resources/static/js/*.js',
        './src/main/resources/static/js/controllers/*.js',
        './src/main/resources/static/js/directives/*.js',
        './src/main/resources/static/js/providers/*.js',
        './src/main/resources/static/js/services/*.js',
        './src/test/resources/static/js/services/*.js'
    ], {read:false});

    return target.pipe(inject(sources, {
        starttag: 'files: [',
        endtag: ']',
        transform: function (filepath, file, i, length) {
            return '  "' + filepath.substring(1, filepath.length) + '"'+ (i + 1 < length ? ',' : '');
        }
    })).pipe(gulp.dest('./'));
});

gulp.task('test', ['testJS'], function (done) {

    new Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
});

gulp.task('run', ['loadCSS']);