(function(angular) {
    'use strict';

    angular.module('stpApp')
        .service('userService', function($q, ngToast, $http, $filter){

            return {
                create : create,
                getAll: getAll,
                update: update,
                unbound: unbound
            };

            function unbound(employee) {
                var deferred = $q.defer();

                $http.patch('/Employee/Unbound/'+employee.id, employee).success(function (data) {
                    ngToast.success($filter('translate')('employee.unbound.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('employee.unbound.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }

            function update(employee){
                var deferred = $q.defer();

                $http.patch('/Employee/Update/'+employee.id, employee).success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    deferred.reject();
                });

                return deferred.promise;
            }

            function getAll() {
                var deferred = $q.defer();

                $http.get('/Employee/All').success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('user.getAll.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }

            function create(user) {
                var deferred = $q.defer();

                $http.post('/Employee/Add', user).success(function (data) {
                    ngToast.success($filter('translate')('user.create.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('user.create.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }


        })

})(angular);


