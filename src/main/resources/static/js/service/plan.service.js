(function(angular) {
    'use strict';

    angular.module('stpApp')
        .service('planService', function($q, ngToast, $http, $filter, buildingService){

            return {
                update : update,
                create : create
            };
            
            function update(plan, building) {
                var deferred = $q.defer();
                for(var i = 0; i < building.plans.length; i++){
                    if (building.plans[i].id == plan.id){
                        building.plans[i] = plan;
                    }
                }
                buildingService.update(building).then(function(data){
                    deferred.resolve(data);
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            }

            function create(plan, building) {
                var deferred = $q.defer();
                building.plans.push(plan);
                buildingService.update(building).then(function(data){
                    ngToast.success($filter('translate')('plan.create.success'));
                    deferred.resolve(data);
                }, function () {
                    ngToast.danger($filter('translate')('plan.create.error'));
                    deferred.reject();
                });
                return deferred.promise;
            }


        })

})(angular);


