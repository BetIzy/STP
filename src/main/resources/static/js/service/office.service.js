(function(angular) {
    'use strict';

    angular.module('stpApp')
        .service('officeService', function($q, ngToast, $http, $filter, planService, userService){

            return {
                displayInstruction: displayInstruction,
                save : save,
                displayAssignmentInstruction : displayAssignmentInstruction,
                displayChangeInstruction : displayChangeInstruction,
                assignEmployee: assignEmployee
            };
            
            function assignEmployee(desk, employee) {
                employee.desk = desk;
                userService.update(employee).then(function (data) {
                    ngToast.sucess($filter('translate')('office.assignEmployee.success'));
                }, function () {
                    ngToast.danger($filter('translate')('office.assignEmployee.error'));
                })
            }

            function displayInstruction() {
                ngToast.info($filter('translate')('office.displayInstruction'));
            }

            function displayAssignmentInstruction(){
                ngToast.info($filter('translate')('office.displayAssignmentInstruction'));
            }

            function displayChangeInstruction(){
                ngToast.info($filter('translate')('office.displayChangeInstruction'));
            }

            function save(matrice, plan, building){
                var deferred = $q.defer();
                var desksToDelete = [];
                if(plan.desks != null){

                }else{
                    plan.desks = [];
                }
                matrice.lines.forEach(function(line){
                    line.columns.forEach(function(column){
                        if(column.occupied){
                            var present = false;
                            plan.desks.forEach(function(desk){
                                if(desk.positionX == column.index && desk.positionY == line.index){
                                    present = true;
                                }
                            });
                            if(!present) {
                                plan.desks.push({
                                    taken: false,
                                    personal: true,
                                    positionX: column.index,
                                    positionY: line.index
                                })
                            }
                        }

                    })
                });


                matrice.lines.forEach(function(line){
                    line.columns.forEach(function(column){
                        if(!column.occupied){
                            var present = [];
                            var i = 0;
                            plan.desks.forEach(function(desk){

                                if(desk.positionX == column.index && desk.positionY == line.index){
                                    present.push(i);
                                    desksToDelete.push(desk);
                                }
                                else{
                                    i++;
                                }
                            });
                            if(present.length > 0) {
                                present.forEach(function (index, j) {
                                    plan.desks.splice(index-j, 1);
                                });
                            }
                        }
                    })
                });
                if(desksToDelete.length > 0) {
                    $http.post('/Desk/Delete', desksToDelete).success(function (data) {
                        ngToast.success($filter('translate')('office.delete.success'));
                    }).error(function () {
                        ngToast.danger($filter('translate')('office.delete.error'));
                    })
                }

                planService.update(plan, building).then(function(data){
                    ngToast.success($filter('translate')('office.add.success'));
                    deferred.resolve(data);
                }, function () {
                    ngToast.danger($filter('translate')('office.add.error'));
                    deferred.reject();
                });
                return deferred.promise;
            }
        })

})(angular);


