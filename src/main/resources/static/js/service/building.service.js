(function(angular) {
    'use strict';

    angular.module('stpApp')
        .service('buildingService', function($q, ngToast, $http, $filter){

            return {
                getAll: getAll,
                update: update,
                create: create
            };

            function create(building) {
                var deferred = $q.defer();

                $http.post('/Building/Add', building).success(function (data) {
                    ngToast.success($filter('translate')('building.create.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('building.create.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }

            function update(building){
                var deferred = $q.defer();
                $http.patch('/Building/Update/'+building.id, building).success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    deferred.reject();
                });
                return deferred.promise;
            }

            function getAll() {
                var deferred = $q.defer();
                $http.get('/Building/All').success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('building.getAll.error'));
                    deferred.reject();
                });
                return deferred.promise;
            }
        })

})(angular);


