(function(angular) {
    'use strict';

    angular.module('stpApp')
        .service('equipmentService', function($q, ngToast, $http, $filter){

            return {
                getAll: getAll,
                update: update,
                create: create,
                unbound: unbound
            };

            function unbound(equipment) {
                var deferred = $q.defer();

                $http.patch('/Equipment/Unbound/'+equipment.id, equipment).success(function (data) {
                    ngToast.success($filter('translate')('equipment.unbound.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('equipment.unbound.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }

            function create(equipment) {
                var deferred = $q.defer();

                $http.post('/Equipment/Add', equipment).success(function (data) {
                    ngToast.success($filter('translate')('equipment.create.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('equipment.create.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }

            function update(equipment) {
                var deferred = $q.defer();

                $http.patch('/Equipment/Update/'+equipment.id, equipment).success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    deferred.reject();
                });

                return deferred.promise;
            }

            function getAll() {
                var deferred = $q.defer();

                $http.get('/Equipment/All').success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('equipment.getAll.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }


        })

})(angular);


