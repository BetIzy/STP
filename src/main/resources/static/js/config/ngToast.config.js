(function(){
    'use strict';

    angular.module('stpApp')
        .config(function(ngToastProvider) {
            ngToastProvider.configure({
                animation: 'slide',
                maxNumber: 3,
                timeout: 5000,
                dismissButton: true
            });
        })
})(angular);
