(function (){

    angular.module('stpApp')
        .config(function ($httpProvider, $stateProvider) {
            $stateProvider
                .state('planSearch', {
                    url: '/planSearch',
                    templateUrl: '/view/planSearch.html',
                    controller: 'planSearchCtrl',
                    controllerAs: 'planSearch'
                })
                .state('planCreation', {
                    url: '/planCreation',
                    templateUrl: '/view/planCreation.html',
                    controller: 'planCreationCtrl',
                    controllerAs: 'planCreation'
                })
                .state('home', {
                    url: '/home',
                    templateUrl: '/view/home.html',
                    controller: 'homeCtrl',
                    controllerAs: 'home'
                })
                .state('officeCreation', {
                    url: '/officeCreation',
                    templateUrl: '/view/officeCreation.html',
                    controller: 'officeCreationCtrl',
                    controllerAs: 'officeCreation'
                })
                .state('officeAssignment', {
                    url: '/officeAssignment',
                    templateUrl: '/view/officeAssignment.html',
                    controller:'officeAssignmentCtrl',
                    controllerAs:'officeAssignment'
                })
                .state('officeChange', {
                    url: '/officeChange',
                    templateUrl: '/view/officeChange.html',
                    controller:'officeChangeCtrl',
                    controllerAs:'officeChange'
                })
                .state('userCreation', {
                    url: '/userCreation',
                    templateUrl: '/view/userCreation.html',
                    controller:'userCreationCtrl',
                    controllerAs:'userCreation'
                })
                .state('equipmentCreation', {
                    url:'/equipmentCreation',
                    templateUrl: '/view/equipmentCreation.html',
                    controller:'equipmentCreationCtrl',
                    controllerAs:'equipmentCreation'
                })
                .state('buildingCreation', {
                    url: '/buildingCreation',
                    templateUrl: '/view/buildingCreation.html',
                    controller:'buildingCreationCtrl',
                    controllerAs:'buildingCreation'
                })
                .state('login', {
                    url: '/login',
                    templateUrl: '/view/login.html',
                    controller: 'loginCtrl',
                    controllerAs: 'login'
                });
        })



})(angular);