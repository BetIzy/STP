(function () {
    'use strict';

    angular.module('stpApp')
        .controller('planSearchCtrl', function($scope, $http, buildingService) {

            var planSearchScope = this;

            planSearchScope.showResult = false;

            planSearchScope.getBuilding = function () {
                buildingService.getAll().then(function (data) {
                    planSearchScope.buildings = data;
                })
            };



            planSearchScope.search = function (){
                if(planSearchScope.searchForm.$valid){
                    planSearchScope.showResult = true;
                }
            }
        })

})(angular);