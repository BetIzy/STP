(function () {
    'use strict';

    angular.module('stpApp')
        .controller('officeAssignmentCtrl', function($scope, $http, buildingService, officeService, userService, equipmentService, ngToast, $filter) {

            var officeAssignementScope = this;

            var lineClicked = null;
            var columnClicked = null;

            officeAssignementScope.save = function () {
                if(officeAssignementScope.userSelected != null){
                    officeAssignementScope.userSelected.desk = officeAssignementScope.deskSelected;
                    userService.update(officeAssignementScope.userSelected).then(function (data) {
                        ngToast.success($filter('translate')('office.assignEmployee.success'));
                    }, function () {
                        ngToast.danger($filter('translate')('office.assignEmployee.error'));
                    })
                }
                if(officeAssignementScope.equipmentSelected != null){
                    officeAssignementScope.equipmentSelected.desk = officeAssignementScope.deskSelected;
                    equipmentService.update(officeAssignementScope.equipmentSelected).then(function (data) {
                        ngToast.success($filter('translate')('office.assignEquipment.success'));
                    }, function () {
                        ngToast.danger($filter('translate')('office.assignEquipment.error'));
                    })
                }
            };
            
            officeAssignementScope.deleteEquipment = function () {
                equipmentService.unbound(officeAssignementScope.deskSelected.equipment).then(function (data) {
                    
                })
            };
            
            officeAssignementScope.deleteEmployee = function () {
                userService.unbound(officeAssignementScope.deskSelected.employee).then(function (data) {

                })
            };


            officeAssignementScope.getBuilding = function () {
                buildingService.getAll().then(function (data) {
                    officeAssignementScope.buildings = data;
                })
            };

            officeAssignementScope.planSelection = function () {
                matrixCreation();
                displayOffice();
                jQuery(document).ready(function() {
                    $('.line').click(function (event) {
                        lineClicked = $(this).attr('name');
                    });
                    $('.column').click(function (event) {
                        columnClicked = $(this).attr('name');
                    });
                    $('.occupied').click(function (event) {
                        $('#officeAssignmentModal').modal('show');
                    });
                });


                officeService.displayAssignmentInstruction();

            };

            officeAssignementScope.matrixClicked = function() {
                if(officeAssignementScope.matrice.lines[lineClicked].columns[columnClicked].occupied){
                    officeAssignementScope.deskSelected = officeAssignementScope.planSelected.desks.filter(function (desk) {
                        return desk.positionY == lineClicked && desk.positionX == columnClicked;
                    })[0];
                }
            };

            officeAssignementScope.getUser = function(){
                userService.getAll().then(function (data) {
                    officeAssignementScope.users = data;
                })
            };

            officeAssignementScope.getEquipment = function () {
                equipmentService.getAll().then(function(data){
                    officeAssignementScope.equipments = data;
                })
            };


            function displayOffice(){
                officeAssignementScope.planSelected.desks.forEach(function(desk){
                    officeAssignementScope.matrice.lines[desk.positionY].columns[desk.positionX].occupied = true;
                })
            }


            function matrixCreation() {
                var i = 0;
                var j = 0;
                officeAssignementScope.matrice = {};
                officeAssignementScope.matrice['lines'] = [];
                for (i = 0; i < Math.trunc(officeAssignementScope.planSelected.length/1.5); i++) {
                    var line = {size: 1.5,
                        index: i,
                        columns: []};
                    officeAssignementScope.matrice.lines.push(line);
                    for (j = 0; j < Math.trunc(officeAssignementScope.planSelected.width/1.5); j++) {
                        var column = {size: 1.5,
                            index: j};
                        officeAssignementScope.matrice.lines[i].columns.push(column);
                    }
                }
            }



        })

})(angular);