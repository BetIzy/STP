(function () {
    'use strict';

    angular.module('stpApp')
        .controller('userCreationCtrl', function($scope, $http, userService) {
            var userCreationScope = this;

            userCreationScope.create = function () {
                if(userCreationScope.createForm.$valid) {
                    var user = {
                        lastName: userCreationScope.lastName,
                        firstName: userCreationScope.firstName,
                        startDate: userCreationScope.date != null ? userCreationScope.date : new Date()
                    };

                    userService.create(user).then(function () {

                    })
                }
            }
        })

})(angular);