(function () {
    'use strict';

    angular.module('stpApp')
        .controller('planCreationCtrl', function($scope, $http, buildingService, planService) {

            var planCreationScope = this;

            planCreationScope.getBuilding = function () {
                buildingService.getAll().then(function (data) {
                    planCreationScope.buildings = data;
                })
            };

            planCreationScope.createPlan = function () {
                if(planCreationScope.createForm.$valid){
                    var plan = {
                        image: null,
                        name: planCreationScope.name,
                        date_version: planCreationScope.date,
                        available_space: planCreationScope.length * planCreationScope.width,
                        length: planCreationScope.length,
                        width: planCreationScope.width,
                        version: planCreationScope.version
                    }
                    planService.create(plan,planCreationScope.buildingSelected).then(function () {

                    });
                }
            }
        })

})(angular);