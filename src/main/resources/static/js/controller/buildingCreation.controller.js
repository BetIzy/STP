(function () {
    'use strict';

    angular.module('stpApp')
        .controller('buildingCreationCtrl', function($scope, $http, buildingService) {
            var buildingCreationScope = this;

            buildingCreationScope.create = function () {
                if(buildingCreationScope.createForm.$valid) {
                    var building = {
                        name: buildingCreationScope.name
                    };

                    buildingService.create(building).then(function () {

                    })
                }
            }
        })

})(angular);