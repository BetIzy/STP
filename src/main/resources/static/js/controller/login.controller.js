(function () {
    'use strict';

    angular.module('stpApp')
        .controller('loginCtrl', function($scope, $http, $rootScope, $location, $filter, ngToast, $state) {

            var loginScope = this;

            var authenticate = function(credentials, callback) {

                var headers = credentials ? {'Authorization' : 'Basic '
                + btoa(credentials.username + ":" + credentials.password)
                } : {};
                $http.get('/user', {headers : headers}).success(function(data) {
                    if (data.name) {
                        $rootScope.authenticated = true;
                    } else {
                        $rootScope.authenticated = false;
                    }
                    callback && callback();
                }).error(function(response) {
                    $rootScope.authenticated = false;
                    callback && callback();
                });

            };

            authenticate();

            loginScope.credentials = {};
            loginScope.login = function() {
                authenticate(loginScope.credentials, function() {
                    if ($rootScope.authenticated) {
                        $state.go("home");
                    } else {
                        $state.go("login");
                        ngToast.info($filter('translate')('user.login.error'));
                    }
                });
            };

        });

})(angular);