(function () {
    'use strict';

    angular.module('stpApp')
        .controller('officeChangeCtrl', function($scope, $http, buildingService, officeService) {

            var officeChangeScope = this;

            var lineClicked = null;
            var columnClicked = null;

            officeChangeScope.save = function () {
                officeService.save(officeChangeScope.matrice, officeChangeScope.planSelected, officeChangeScope.buildingSelected).then(function(){

                });
            };


            officeChangeScope.getBuilding = function () {
                buildingService.getAll().then(function (data) {
                    officeChangeScope.buildings = data;
                })
            };

            officeChangeScope.planSelection = function () {
                matrixCreation();
                displayOffice();
                jQuery(document).ready(function() {
                    $('.line').click(function (event) {
                        lineClicked = $(this).attr('name');
                    });
                    $('.column').click(function (event) {
                        columnClicked = $(this).attr('name');
                    });
                });
                officeService.displayChangeInstruction();
            };

            officeChangeScope.matrixClicked = function() {
                if(officeChangeScope.matrice.lines[lineClicked].columns[columnClicked].occupied){
                    officeChangeScope.matrice.lines[lineClicked].columns[columnClicked].occupied = false;
                }else{
                    officeChangeScope.matrice.lines[lineClicked].columns[columnClicked].occupied = true;
                }
            };

            function displayOffice(){
                officeChangeScope.planSelected.desks.forEach(function(desk){
                    officeChangeScope.matrice.lines[desk.positionY].columns[desk.positionX].occupied = true;
                })
            }


            function matrixCreation() {
                var i = 0;
                var j = 0;
                officeChangeScope.matrice = {};
                officeChangeScope.matrice['lines'] = [];
                for (i = 0; i < Math.trunc(officeChangeScope.planSelected.length/1.5); i++) {
                    var line = {size: 1.5,
                        index: i,
                        columns: []};
                    officeChangeScope.matrice.lines.push(line);
                    for (j = 0; j < Math.trunc(officeChangeScope.planSelected.width/1.5); j++) {
                        var column = {size: 1.5,
                            index: j};
                        officeChangeScope.matrice.lines[i].columns.push(column);
                    }
                }

            }


        })

})(angular);