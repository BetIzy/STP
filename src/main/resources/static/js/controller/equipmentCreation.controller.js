(function () {
    'use strict';

    angular.module('stpApp')
        .controller('equipmentCreationCtrl', function($scope, $http, equipmentService) {
            var equipmentCreationScope = this;

            equipmentCreationScope.create = function () {
                if(equipmentCreationScope.createForm.$valid) {
                    var equipment = {
                        name: equipmentCreationScope.name
                    };

                    equipmentService.create(equipment).then(function () {

                    })
                }
            }
        })

})(angular);