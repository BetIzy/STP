(function () {
    'use strict';

    angular.module('stpApp')
        .controller('officeCreationCtrl', function($scope, $http, buildingService, officeService) {

            var officeCreationScope = this;

            var lineClicked = null;
            var columnClicked = null;

            officeCreationScope.save = function () {
                officeService.save(officeCreationScope.matrice, officeCreationScope.planSelected, officeCreationScope.buildingSelected).then(function(){

                });
            };


            officeCreationScope.getBuilding = function () {
                buildingService.getAll().then(function (data) {
                    officeCreationScope.buildings = data;
                })
            };

            officeCreationScope.planSelection = function () {
                matrixCreation();
                jQuery(document).ready(function() {
                    $('.line').click(function (event) {
                        lineClicked = $(this).attr('name');
                    });
                    $('.column').click(function (event) {
                        columnClicked = $(this).attr('name');
                    });

                });
                officeService.displayInstruction();
            };

            officeCreationScope.matrixClicked = function() {
                if(officeCreationScope.matrice.lines[lineClicked].columns[columnClicked].occupied){
                    officeCreationScope.matrice.lines[lineClicked].columns[columnClicked].occupied = false;
                }else{
                    officeCreationScope.matrice.lines[lineClicked].columns[columnClicked].occupied = true;
                }
            };


            function matrixCreation() {
                var i = 0;
                var j = 0;
                officeCreationScope.matrice = {};
                officeCreationScope.matrice['lines'] = [];
                for (i = 0; i < Math.trunc(officeCreationScope.planSelected.length/1.5); i++) {
                    var line = {size: 1.5,
                    index: i,
                    columns: []};
                    officeCreationScope.matrice.lines.push(line);
                    for (j = 0; j < Math.trunc(officeCreationScope.planSelected.width/1.5); j++) {
                        var column = {size: 1.5,
                            index: j};
                        officeCreationScope.matrice.lines[i].columns.push(column);
                    }
                }

            }


        })

})(angular);