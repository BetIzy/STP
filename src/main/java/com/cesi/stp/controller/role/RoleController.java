package com.cesi.stp.controller.role;

import com.cesi.stp.dao.Role.RoleDAO;
import com.cesi.stp.entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/Role")
@RestController
public class RoleController {

    @Autowired
    private RoleDAO roleDAO;

    @RequestMapping("/{id}")
    @ResponseBody
    public Role findRoleById(@PathVariable int id){return roleDAO.findById(id);}

    @RequestMapping("/All")
    @ResponseBody
    public List<Role> findAllRole(){return roleDAO.findAll();}

    @PostMapping("/Add")
    @ResponseBody
    public int insertRole(@RequestBody Role role){
        Role roleRetour = roleDAO.save(role);
        if(roleRetour != null) {
            return roleRetour.getId();
        } else {
            return -1;
        }
    }

    public RoleDAO getRoleDAO() {
        return roleDAO;
    }

    public void setRoleDAO(RoleDAO roleDAO) {
        this.roleDAO = roleDAO;
    }
}

