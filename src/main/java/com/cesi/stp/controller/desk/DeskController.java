package com.cesi.stp.controller.desk;

import com.cesi.stp.dao.desk.DeskDAO;
import com.cesi.stp.dao.employee.EmployeeDAO;
import com.cesi.stp.dao.equipment.EquipmentDAO;
import com.cesi.stp.entity.Desk;
import com.cesi.stp.entity.Employee;
import com.cesi.stp.entity.Equipment;
import com.cesi.stp.service.CascadeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bjorn on 28/08/2017.
 */
@RequestMapping("/Desk")
@RestController
public class DeskController {

    @Autowired
    DeskDAO deskDAO;

    @Autowired
    EquipmentDAO equipmentDAO;

    @Autowired
    EmployeeDAO employeeDAO;

    @RequestMapping("/{id}")
    @ResponseBody
    public Desk findDeskById(@PathVariable int id){return CascadeUtils.modifyDeskToEquipmentEmployee(deskDAO.findById(id), true);}

    @RequestMapping("/All")
    @ResponseBody
    public List<Desk> findAllDesk(){return deskDAO.findAll();}

    @PostMapping("/Add")
    public List<Integer> insertDesk(@RequestBody List<Desk> desks){
        return saveDesk(desks);
    }

    @PatchMapping("/Update/{id}")
    @ResponseBody
    public int updateDesk(@RequestBody List<Desk> desks, @PathVariable int id){
        saveDesk(desks);
        return 0;
    }

    @PostMapping("/Delete")
    public void deleteDesk(@RequestBody List<Desk> desks){
        for(Desk desk : desks) {

            Employee employee = employeeDAO.findByDesk_id(desk.getId());
            Equipment equipment = equipmentDAO.findByDesk_id(desk.getId());

            if(employee != null){
                employee.setDesk(null);
                employeeDAO.save(employee);
            }
            if(equipment != null){
                equipment.setDesk(null);
                equipmentDAO.save(equipment);
            }

            desk.setEmployee(null);
            desk.setEquipment(null);
            deskDAO.deleteById(desk.getId());
        }
    }

    private List<Integer> saveDesk(List<Desk> desks){
        ArrayList<Integer> idList = new ArrayList<Integer>();
        if(desks != null && !desks.isEmpty()){
            for(int i=0; i<desks.size(); i++){
                idList.add(deskDAO.save(desks.get(i)).getId());
            }
            return idList;
        } else {
            idList.add(-1);
            return idList;
        }
    }

    public DeskDAO getDeskDAO() {
        return deskDAO;
    }

    public void setDeskDAO(DeskDAO deskDAO) {
        this.deskDAO = deskDAO;
    }
}
