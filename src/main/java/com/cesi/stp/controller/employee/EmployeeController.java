package com.cesi.stp.controller.employee;

import com.cesi.stp.dao.employee.EmployeeDAO;
import com.cesi.stp.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by bjorn on 27/07/2017.
 */
@RequestMapping("/Employee")
@RestController
public class EmployeeController {

    @Autowired
    private EmployeeDAO employeeDAO;

    @RequestMapping("/{id}")
    @ResponseBody
    public Employee findEmployeeById(@PathVariable int id){return employeeDAO.findById(id);}

    @RequestMapping("/All")
    @ResponseBody
    public List<Employee> findAllEmployee(){
        List<Employee> employeeList = employeeDAO.findAll();
        for(Employee employee: employeeList){
            employee.setDesk(null);
        }
        return employeeList;
    }

    @PostMapping("/Add")
    @ResponseBody
    public int insertEmployee(@RequestBody Employee employee){
       return saveEmployee(employee);
    }

    @PatchMapping("/Update/{id}")
    @ResponseBody
    public int updateEmployee(@RequestBody Employee employee, @PathVariable int id){
        return saveEmployee(employee);
    }

    @PatchMapping("/Unbound/{id}")
    public void unboundEmployee(@RequestBody Employee employee, @PathVariable int id){
        if(id == employee.getId()){
            employee.setDesk(null);
            saveEmployee(employee);
        }
    }

    private int saveEmployee(Employee employee){
        Employee employeeRetour = employeeDAO.save(employee);
        if(employeeRetour != null) {
            return employeeRetour.getId();
        } else {
            return -1;
        }
    }

    public EmployeeDAO getEmployeeDAO() {
        return employeeDAO;
    }

    public void setEmployeeDAO(EmployeeDAO employeeDAO) {
        this.employeeDAO = employeeDAO;
    }
}
