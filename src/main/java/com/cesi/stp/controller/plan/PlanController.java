package com.cesi.stp.controller.plan;

import com.cesi.stp.dao.plan.PlanDAO;
import com.cesi.stp.entity.Plan;
import com.cesi.stp.service.CascadeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by bjorn on 28/08/2017.
 */
@RequestMapping("/Plan")
@RestController
public class PlanController {

    @Autowired
    private PlanDAO planDAO;

    @RequestMapping("/{id}")
    @ResponseBody
    public Plan findPlanById(@PathVariable int id){
        return CascadeUtils.modifyPlanToDesk(planDAO.findById(id), true);
    }

    @RequestMapping("/All")
    @ResponseBody
    public List<Plan> findAllPlan(){
        List<Plan> planList = planDAO.findAll();
        for (Plan plan : planList){
            plan = CascadeUtils.modifyPlanToDesk(plan, true);
        }
        return planList;
    }

    @RequestMapping("/No/Desk")
    @ResponseBody
    public List<Plan> findPlanWithNoDesk(){
        return planDAO.findByDesksIsNull();
    }

    @PostMapping("/Add")
    @ResponseBody
    public int insertPlan(@RequestBody Plan plan){
        return savePlan(plan);
    }

    @PatchMapping("/Update/{id}")
    @ResponseBody
    public int updatePlan(@RequestBody Plan plan, @PathVariable int id){
        return savePlan(plan);
    }

    private int savePlan(Plan plan){
        plan = CascadeUtils.modifyPlanToDesk(plan, false);
        Plan planRetour = planDAO.save(plan);
        if(planRetour != null) {
            return planRetour.getId();
        } else {
            return -1;
        }
    }



    public PlanDAO getPlanDAO() {
        return planDAO;
    }

    public void setPlanDAO(PlanDAO planDAO) {
        this.planDAO = planDAO;
    }
}
