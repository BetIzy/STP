package com.cesi.stp.controller.equipment;

import com.cesi.stp.dao.equipment.EquipmentDAO;
import com.cesi.stp.entity.Equipment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by bjorn on 28/08/2017.
 */
@RequestMapping("/Equipment")
@RestController
public class EquipmentController {
    @Autowired
    private EquipmentDAO equipmentDAO;

    @RequestMapping("/{id}")
    @ResponseBody
    public Equipment findEquipmentById(@PathVariable int id){return equipmentDAO.findById(id);}

    @RequestMapping("/All")
    @ResponseBody
    public List<Equipment> findAllEquipment(){
        List<Equipment> equipmentList = equipmentDAO.findAll();
        for(Equipment equipment: equipmentList){
            equipment.setDesk(null);
        }
        return  equipmentList;
    }

    @RequestMapping(value = "/Add")
    @ResponseBody
    public int insertEquipment(@RequestBody Equipment equipment){
        return saveEquipment(equipment);
    }

    @PatchMapping("/Update/{id}")
    @ResponseBody
    public int updateEquipment(@RequestBody Equipment equipment, @PathVariable int id){
        return saveEquipment(equipment);
    }

    @PatchMapping("/Unbound/{id}")
    public void unboundEquipment(@RequestBody Equipment equipment, @PathVariable int id){
        if(id == equipment.getId()){
            equipment.setDesk(null);
            saveEquipment(equipment);
        }
    }

    private int saveEquipment(Equipment equipment){
        Equipment equipmentRetour = equipmentDAO.save(equipment);
        if(equipmentRetour != null) {
            return equipmentRetour.getId();
        } else {
            return -1;
        }
    }

    public EquipmentDAO getEquipmentDAO() {
        return equipmentDAO;
    }

    public void setEquipmentDAO(EquipmentDAO equipmentDAO) {
        this.equipmentDAO = equipmentDAO;
    }
}
