package com.cesi.stp.controller.building;

import com.cesi.stp.dao.building.BuildingDAO;
import com.cesi.stp.entity.Building;
import com.cesi.stp.service.CascadeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/Building")
@RestController
public class BuildingController {

    @Autowired
    BuildingDAO buildingDAO;

    @RequestMapping("/{id}")
    @ResponseBody
    public Building findBuildingById(@PathVariable int id){
        return CascadeUtils.modifyBuildingToPlan(buildingDAO.findById(id), true);
    }

    @RequestMapping("/All")
    @ResponseBody
    public List<Building> findAllBuilding(){
        List<Building> buildingList = buildingDAO.findAll();
        for(Building building : buildingList){
            building = CascadeUtils.modifyBuildingToPlan(building, true);
        }
        return buildingList;
    }

    @PostMapping("/Add")
    @ResponseBody
    public int insertBuilding(@RequestBody Building building){
        return saveBuilding(building);
    }

    @PatchMapping("/Update/{id}")
    @ResponseBody
    public int updateBuilding(@RequestBody Building building, @PathVariable int id){
        if(building.getId() == id){
            building = CascadeUtils.modifyBuildingToPlan(building, false);
            return saveBuilding(building);
        } else {
            return -1;
        }
    }

    private int saveBuilding(Building building){
        Building buildingReturn = buildingDAO.save(building);
        if(buildingReturn != null) {
            return buildingReturn.getId();
        } else {
            return -1;
        }
    }

    public BuildingDAO getBuildingDAO() {
        return buildingDAO;
    }

    public void setBuildingDAO(BuildingDAO buildingDAO) {
        this.buildingDAO = buildingDAO;
    }
}
