package com.cesi.stp.controller.user;

import com.cesi.stp.dao.User.UserDAO;
import com.cesi.stp.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequestMapping("/User")
@Controller
public class UserController {

    @Autowired
    private UserDAO userDAO;

    @RequestMapping("/{id}")
    @ResponseBody
    public User findUserById(@PathVariable int id){return userDAO.findById(id);}

    @RequestMapping("/All")
    @ResponseBody
    public List<User> findAllUser(){return userDAO.findAll();}

    @RequestMapping(value = "/Add")
    @ResponseBody
    public int insertUser(@RequestBody User user){
        User userRetour = userDAO.save(user);
        if(userRetour != null) {
            return userRetour.getId();
        } else {
            return -1;
        }
    }

    public UserDAO getUserDAO() {
        return userDAO;
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
}
