package com.cesi.stp.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by bjorn on 28/08/2017.
 */
@Entity
@Table(name="EQUIPMENT")
public class Equipment implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(unique = true, name = "equipment_id", nullable = false, length=11)
    private int id;

    @Column(name="number", length = 25)
    private String number;

    @Column(name="occupied_surface")
    private Float occupied_surface;

    @Column(name="category")
    private String category;

    @Column(name = "name")
    private String name;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "desk_id")
    private Desk desk;

    public Equipment(){
    }

    public Equipment(String number, Float occupied_surface, String category, String name, Desk desk) {
        this.number = number;
        this.occupied_surface = occupied_surface;
        this.category = category;
        this.name = name;
        this.desk = desk;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Float getOccupied_surface() {
        return occupied_surface;
    }

    public void setOccupied_surface(Float occupied_surface) {
        this.occupied_surface = occupied_surface;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Desk getDesk() {
        return desk;
    }

    public void setDesk(Desk desk) {
        this.desk = desk;
    }
}
