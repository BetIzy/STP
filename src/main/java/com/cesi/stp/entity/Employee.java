package com.cesi.stp.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by bjorn on 27/07/2017.
 */
@Entity
@Table(name="employee")
public class Employee implements Serializable{

    //Identifiant unique de l'employé
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(unique = true, name = "employee_id", nullable = false, length=11)
    private int id;

    //Nom de famille de l'employé
    @Column( name = "last_name", nullable = false, length=25)
    private String lastName;

    //Prénom de l'employé
    @Column( name = "first_name", nullable = false, length=25)
    private String firstName;

    //Date d'arrivée de l'employé
    @Column( name = "start_date", nullable = false)
    private Date startDate;

    //Date de départ de l'employé
    @Column( name = "end_date")
    private Date endDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "desk_id")
    private Desk desk;

    public Employee() {
    }

    public Employee(String lastName, String firstName, Date startDate, Date endDate) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Desk getDesk() {
        return desk;
    }

    public void setDesk(Desk desk) {
        this.desk = desk;
    }
}
