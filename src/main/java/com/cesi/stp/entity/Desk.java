package com.cesi.stp.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by bjorn on 28/07/2017.
 */
@Entity
@Table(name="desk")
public class Desk  implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(unique = true, name = "desk_id", nullable = false, length=11)
    private int id;

    @Column( name = "taken", nullable = false)
    private Boolean taken;

    @Column( name = "personal", nullable = false)
    private Boolean personal;

    @Column( name = "position_x")
    private int positionX;

    @Column( name = "position_y")
    private int positionY;

    @Column( name = "orientation")
    private String orientation;

    @OneToOne(mappedBy = "desk",fetch = FetchType.LAZY, cascade={CascadeType.ALL})
    private Employee employee;

    @OneToOne(mappedBy = "desk",fetch = FetchType.LAZY, cascade={CascadeType.ALL})
    private Equipment equipment;

    @ManyToOne
    @JoinColumn(name="plan_id")
    private Plan plan;

    public Desk() {
    }

    public Desk(Boolean taken, Boolean personal, Employee employee) {
        this.taken = taken;
        this.personal = personal;
        this.employee = employee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getTaken() {
        return taken;
    }

    public void setTaken(Boolean taken) {
        this.taken = taken;
    }

    public Boolean getPersonal() {
        return personal;
    }

    public void setPersonal(Boolean personnal) {
        this.personal = personnal;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }
}
