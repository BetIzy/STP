package com.cesi.stp.entity;

import javax.persistence.*;

@Entity
@Table(name="Role")
public class Role {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(unique = true, name = "role_id", nullable = false, length=11)
    private int id;

    @Column( name = "name", nullable = false, length = 25)
    private String name;

    @ManyToOne
    @JoinColumn(name="user_id", nullable=true)
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
