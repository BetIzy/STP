package com.cesi.stp.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by bjorn on 28/08/2017.
 */
@Entity
@Table(name = "plan")
public class Plan implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(unique = true, name = "plan_id", nullable = false, length=11)
    private int id;

    @Column( name = "image", nullable = false, length=25)
    private String image;

    @Column(name = "name", length = 25)
    private String name;

    @Column(name = "date_version", nullable = false)
    private Date date_version;

    @Column(name = "available_space", nullable = false)
    private Float available_space;

    @Column(name = "length", nullable = false)
    private Float length;

    @Column(name = "width", nullable = false)
    private Float width;

    @Column(name = "version", nullable = false)
    private String version;

    @OneToMany(mappedBy = "plan", fetch = FetchType.LAZY, cascade={CascadeType.ALL})
    private List<Desk> desks;

    @OneToMany(mappedBy = "plan",fetch = FetchType.LAZY, cascade={CascadeType.ALL})
    private List<User> users;

    @ManyToOne
    @JoinColumn(name="building_id")
    private Building building;

    public Plan() {
    }

    public Plan(String image, String name, Date date_version, Float available_space) {
        this.image = image;
        this.name = name;
        this.date_version = date_version;
        this.available_space = available_space;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate_version() {
        return date_version;
    }

    public void setDate_version(Date date_version) {
        this.date_version = date_version;
    }

    public Float getAvailable_space() {
        return available_space;
    }

    public void setAvailable_space(Float available_space) {
        this.available_space = available_space;
    }

    public List<Desk> getDesks() {
        return desks;
    }

    public void setDesks(List<Desk> desks) {
        this.desks = desks;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }
}
