package com.cesi.stp.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="building")
public class Building {

        @Id
        @GeneratedValue(strategy= GenerationType.AUTO)
        @Column(unique = true, name = "building_id", nullable = false, length=11)
        private int id;

        @Column(name="name", length = 25)
        private String name;

    @OneToMany(mappedBy = "building",fetch = FetchType.LAZY, cascade={CascadeType.ALL})
    private List<Plan> plans;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Plan> getPlans() {
        return plans;
    }

    public void setPlans(List<Plan> plans) {
        this.plans = plans;
    }
}
