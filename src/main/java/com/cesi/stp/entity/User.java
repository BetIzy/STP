package com.cesi.stp.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(unique = true, name = "user_id", nullable = false, length=11)
    private int id;

    @Column( name = "login", nullable = false, length = 25)
    private String login;

    @ManyToOne
    @JoinColumn(name="plan_id", nullable=true)
    private Plan plan;

    @OneToMany(mappedBy = "user")
    private List<Role> roles;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
