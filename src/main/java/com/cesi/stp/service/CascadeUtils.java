package com.cesi.stp.service;

import com.cesi.stp.entity.*;

import java.util.List;

public class CascadeUtils {

    /**
     *
     * @param building
     * @param isRemove
     * @return
     */
    public static Building modifyBuildingToPlan(Building building, boolean isRemove){
        List<Plan> plansList = building.getPlans();
        if(plansList != null && !plansList.isEmpty()) {
            for (Plan plan : plansList) {
                plan = modifyPlanToDesk(plan, isRemove);
                if (isRemove) {
                    plan.setBuilding(null);
                } else {
                    plan.setBuilding(building);
                }
            }
            building.setPlans(plansList);
        }
        return building;
    }

    public static Plan modifyPlanToDesk(Plan plan, boolean isRemove){
        List<Desk> desksList = plan.getDesks();
        if(isRemove){
            plan.setBuilding(null);
        }
        if(desksList != null && !desksList.isEmpty()) {
            for (Desk desk : desksList) {
                desk = modifyDeskToEquipmentEmployee(desk, isRemove);
                if (isRemove) {
                    desk.setPlan(null);
                } else {
                    desk.setPlan(plan);
                }
            }
            plan.setDesks(desksList);
        }
        return plan;
    }

    public static Desk modifyDeskToEquipmentEmployee(Desk desk, boolean isRemove){
        if(isRemove){
            desk.setPlan(null);
            if(desk.getEquipment() != null)
                desk.getEquipment().setDesk(null);
            if(desk.getEmployee() != null)
                desk.getEmployee().setDesk(null);
        } else {
            if(desk.getEmployee() != null){
                desk.getEmployee().setDesk(desk);
            }
            if(desk.getEquipment() != null) {
                desk.getEquipment().setDesk(desk);
            }
        }
        return desk;
    }
}
