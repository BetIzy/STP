package com.cesi.stp.dao.employee;

import com.cesi.stp.entity.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by bjorn on 27/07/2017.
 */
@Transactional
public interface EmployeeDAO extends CrudRepository<Employee, Integer>{
    public Employee findById(int id);

    public List<Employee> findAll();

    public Employee save(Employee employee);

    public Employee findByDesk_id(int deskId);

    public int deleteById(int id);

}
