package com.cesi.stp.dao.equipment;

import com.cesi.stp.entity.Equipment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by bjorn on 28/08/2017.
 */
@Transactional
public interface EquipmentDAO extends CrudRepository<Equipment, Integer>{
    public Equipment findById(int id);

    public List<Equipment> findAll();

    public Equipment save(Equipment equipment);

    public Equipment findByDesk_id(int deskId);

    public int deleteById(int id);
}
