package com.cesi.stp.dao.building;

import com.cesi.stp.entity.Building;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface BuildingDAO extends CrudRepository<Building, Integer> {
    public Building findById(int id);

    public List<Building> findAll();

    public Building save(Building building);
}
