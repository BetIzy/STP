package com.cesi.stp.dao.Role;

import com.cesi.stp.entity.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface RoleDAO extends CrudRepository<Role, Integer>{
    public Role findById(int id);

    public List<Role> findAll();

    public Role save(Role role);
}
