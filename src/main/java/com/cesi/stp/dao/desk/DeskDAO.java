package com.cesi.stp.dao.desk;

import com.cesi.stp.entity.Desk;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by bjorn on 28/08/2017.
 */
@Transactional
public interface DeskDAO extends CrudRepository<Desk, Integer> {
    public Desk findById(int id);

    public List<Desk> findAll();

    public Desk save(Desk desk);

    public int deleteById(int id);
}
