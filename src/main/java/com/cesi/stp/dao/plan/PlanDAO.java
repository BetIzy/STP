package com.cesi.stp.dao.plan;

import com.cesi.stp.entity.Plan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by bjorn on 28/08/2017.
 */
@Transactional
public interface PlanDAO extends CrudRepository<Plan, Integer>{
    public Plan findById(int id);

    public List<Plan> findAll();

    public Plan save(Plan plan);

    public List<Plan> findByDesksIsNull();
}
