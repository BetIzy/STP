package com.cesi.stp.dao.User;

import com.cesi.stp.entity.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface UserDAO extends CrudRepository<User, Integer>{
    public User findById(int id);

    public List<User> findAll();

    public User save(User user);
}
