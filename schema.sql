#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Plan
#------------------------------------------------------------

CREATE TABLE Plan(
        id              int (11) Auto_increment  NOT NULL ,
        image           Blob NOT NULL ,
        name            Varchar (25) ,
        date_version    Date NOT NULL ,
        available_space Float NOT NULL ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Desk
#------------------------------------------------------------

CREATE TABLE Desk(
        id       int (11) Auto_increment  NOT NULL ,
        taken Bool NOT NULL ,
        personal Bool NOT NULL ,
        id_Plan  Int NOT NULL ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Employee
#------------------------------------------------------------

CREATE TABLE Employee(
        id         int (11) Auto_increment  NOT NULL ,
        last_name  Varchar (25) NOT NULL ,
        first_name Varchar (25) NOT NULL ,
        start_date Date ,
        end_date  Date ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Equipment
#------------------------------------------------------------

CREATE TABLE Equipment(
        id               int (11) Auto_increment  NOT NULL ,
        number           Varchar (25) ,
        occupied_surface Float NOT NULL ,
        category         Varchar (25) NOT NULL ,
        name             Varchar (25) ,
        id_Desk          Int NOT NULL ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: User
#------------------------------------------------------------

CREATE TABLE User(
        id      int (11) Auto_increment  NOT NULL ,
        login   Varchar (25) NOT NULL ,
        id_Role Int NOT NULL ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Role
#------------------------------------------------------------

CREATE TABLE Role(
        id   int (11) Auto_increment  NOT NULL ,
        name Varchar (25) NOT NULL ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Own
#------------------------------------------------------------

CREATE TABLE Own(
        id          Int NOT NULL ,
        id_Employee Int NOT NULL ,
        PRIMARY KEY (id ,id_Employee )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Interfere
#------------------------------------------------------------

CREATE TABLE Interfere(
        rights  Varchar (25) ,
        id      Int NOT NULL ,
        id_Plan Int NOT NULL ,
        PRIMARY KEY (id ,id_Plan )
)ENGINE=InnoDB;

ALTER TABLE Desk ADD CONSTRAINT FK_Desk_id_Plan FOREIGN KEY (id_Plan) REFERENCES Plan(id);
ALTER TABLE Equipment ADD CONSTRAINT FK_Equipment_id_Desk FOREIGN KEY (id_Desk) REFERENCES Desk(id);
ALTER TABLE User ADD CONSTRAINT FK_User_id_Role FOREIGN KEY (id_Role) REFERENCES Role(id);
ALTER TABLE Own ADD CONSTRAINT FK_Own_id FOREIGN KEY (id) REFERENCES Desk(id);
ALTER TABLE Own ADD CONSTRAINT FK_Own_id_Employee FOREIGN KEY (id_Employee) REFERENCES Employee(id);
ALTER TABLE Interfere ADD CONSTRAINT FK_Interfere_id FOREIGN KEY (id) REFERENCES User(id);
ALTER TABLE Interfere ADD CONSTRAINT FK_Interfere_id_Plan FOREIGN KEY (id_Plan) REFERENCES Plan(id);


-- MAJ Employee 28/07/2017 BDA
ALTER TABLE `employee`
	ALTER `start_date` DROP DEFAULT;
ALTER TABLE `employee`
	CHANGE COLUMN `start_date` `start_date` DATE NOT NULL AFTER `first_name`;

ALTER TABLE `employee`
	ADD COLUMN `desk_id` INT NULL;

ALTER TABLE `employee`
	ADD CONSTRAINT `FK_desk_id` FOREIGN KEY (`desk_id`) REFERENCES `desk` (`id`);

-- END MAJ