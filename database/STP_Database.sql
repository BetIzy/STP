-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.2.6-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Export de la structure de la table stp_dev. desk
CREATE TABLE IF NOT EXISTS `desk` (
  `desk_id` int(11) NOT NULL AUTO_INCREMENT,
  `taken` tinyint(1) NOT NULL,
  `personal` tinyint(1) NOT NULL,
  `plan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`desk_id`),
  KEY `FK_plan_id` (`plan_id`),
  CONSTRAINT `FK_plan_id` FOREIGN KEY (`plan_id`) REFERENCES `plan` (`plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table stp_dev. employee
CREATE TABLE IF NOT EXISTS `employee` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(25) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `desk_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `FK_desk_id` (`desk_id`),
  CONSTRAINT `FK_desk_id` FOREIGN KEY (`desk_id`) REFERENCES `desk` (`desk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table stp_dev. equipment
CREATE TABLE IF NOT EXISTS `equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(25) DEFAULT NULL,
  `occupied_surface` float NOT NULL,
  `category` varchar(25) NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  `desk_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Equipment_id_Desk` (`desk_id`),
  CONSTRAINT `FK_Equipment_id_Desk` FOREIGN KEY (`desk_id`) REFERENCES `desk` (`desk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table stp_dev. interfere
CREATE TABLE IF NOT EXISTS `interfere` (
  `rights` varchar(25) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_plan` int(11) NOT NULL,
  PRIMARY KEY (`id_user`,`id_plan`),
  KEY `FK_Interfere_id_Plan` (`id_plan`),
  CONSTRAINT `FK_Interfere_id` FOREIGN KEY (`id_User`) REFERENCES `user` (`user_id`),
  CONSTRAINT `FK_Interfere_id_Plan` FOREIGN KEY (`id_Plan`) REFERENCES `plan` (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table stp_dev. plan
CREATE TABLE IF NOT EXISTS `plan` (
  `plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  `date_version` date NOT NULL,
  `available_space` float NOT NULL,
  PRIMARY KEY (`plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table stp_dev. role
CREATE TABLE IF NOT EXISTS `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table stp_dev. user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(25) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `FK_User_id_Role` (`role_id`),
  CONSTRAINT `FK_User_id_Role` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
